/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author frederik
 */
public class Labor3Test {

    private static Connection connection;
    private static Statement statement;

    public Labor3Test() {

    }

    @BeforeClass
    public static void setUpClass() {
        /*
        createTestDB();
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdatabase", "dbadmin", "dbadminpassword");
            statement = connection.createStatement();
        } catch (SQLException e) {
            System.err.println(e.toString());
            fail();
        }
        */
    }

    @AfterClass
    public static void tearDownClass() {
        /*
        try {
            connection.close();

        } catch (SQLException e) {
            System.err.println(e.toString());
            fail();
        }
        deleteTestDB();
        */
    }

    @Test
    public void test() {
        /*
        createDBScheme();
        insertTestData();

        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM Persons;");
            while (rs.next()) {
                System.out.println(rs.getInt(1));
                System.out.println(rs.getString(2));
                System.out.println(rs.getString(3));
                System.out.println(rs.getString(4));
                System.out.println(rs.getString(5));

                assertEquals(0, rs.getInt(1));
                assertEquals("Ruehl", rs.getString(2));
                assertEquals("Stefan", rs.getString(3));
                assertEquals("idk", rs.getString(4));
                assertEquals("idk", rs.getString(5));
            }
            
            System.out.println("got it!");
        } catch (SQLException ex) {
            System.err.println("verkackt...");
            Logger.getLogger(Labor3Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
        assertTrue(true);
    }

    private void insertData() throws SQLException {

    }

    private void createDBScheme() {
        try {
            statement.execute("CREATE TABLE Persons (\n"
                    + "    PersonID int,\n"
                    + "    LastName varchar(255),\n"
                    + "    FirstName varchar(255),\n"
                    + "    Address varchar(255),\n"
                    + "    City varchar(255)\n"
                    + ");");
        } catch (SQLException ex) {
            Logger.getLogger(Labor3Test.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
    }

    private void insertTestData() {
        try {
            statement.execute("insert into Persons (PersonID, LastName, FirstName, Address, City) Values (0, 'Ruehl', 'Stefan', 'idk', 'idk');");
        } catch (SQLException ex) {
            Logger.getLogger(Labor3Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void createTestDB() {
        try {
            Connection localConnection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "dbadmin", "dbadminpassword");
            Statement localStatement = localConnection.createStatement();
            localStatement.execute("CREATE DATABASE testDatabase");
            localConnection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Labor3Test.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
    }

    private static void deleteTestDB() {
        Connection localConnection;
        try {
            localConnection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "dbadmin", "dbadminpassword");
            Statement localStatement = localConnection.createStatement();
            localStatement.execute("DROP DATABASE testDatabase");
            localConnection.close();
        } catch (SQLException ex) {
            System.err.println("i failed here");
            Logger.getLogger(Labor3Test.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }

    }
}
